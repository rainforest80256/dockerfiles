# .zshrc Source Basic
source ~/.antigen/antigen.zsh

# Load the oh-my-zsh's library.
antigen use oh-my-zsh

# Bundles from the default repo (robbyrussell's oh-my-zsh).
antigen bundle git
antigen bundle rsync
antigen bundle heroku
antigen bundle pip
antigen bundle python
antigen bundle history
antigen bundle command-not-found

# Third Party
antigen bundle kennethreitz/autoenv

# Syntax highlighting bundle.
antigen bundle zsh-users/zsh-completions src
antigen bundle zsh-users/zsh-syntax-highlighting
antigen bundle zsh-users/zsh-autosuggestions
antigen bundle zsh-users/zsh-history-substring-search

# Load the theme.
# antigen theme https://github.com/carloscuesta/materialshell  /zsh/materialshell-dark
# antigen theme https://github.com/denysdovhan/spaceship-zsh-theme spaceship
# antigen theme geometry-zsh/geometry

# Tell antigen that you're done.
antigen apply

source "${HOME}/.aliases"
