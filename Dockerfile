FROM ubuntu:14.04

RUN \
    apt update && \ 
    apt install -y zsh curl git vim
RUN curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh -O - | zsh || true
COPY .zshrc /root/.zshrc
RUN \
    cd && \
    touch .aliases && \
    mkdir .antigen && curl -L git.io/antigen > .antigen/antigen.zsh
CMD ["zsh"]